# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "agitr_chapter8_plus: 0 messages, 2 services")

set(MSG_I_FLAGS "-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(agitr_chapter8_plus_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv" NAME_WE)
add_custom_target(_agitr_chapter8_plus_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "agitr_chapter8_plus" "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv" ""
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv" NAME_WE)
add_custom_target(_agitr_chapter8_plus_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "agitr_chapter8_plus" "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services
_generate_srv_cpp(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/agitr_chapter8_plus
)
_generate_srv_cpp(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/agitr_chapter8_plus
)

### Generating Module File
_generate_module_cpp(agitr_chapter8_plus
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/agitr_chapter8_plus
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(agitr_chapter8_plus_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(agitr_chapter8_plus_generate_messages agitr_chapter8_plus_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_cpp _agitr_chapter8_plus_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_cpp _agitr_chapter8_plus_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(agitr_chapter8_plus_gencpp)
add_dependencies(agitr_chapter8_plus_gencpp agitr_chapter8_plus_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS agitr_chapter8_plus_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services
_generate_srv_eus(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/agitr_chapter8_plus
)
_generate_srv_eus(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/agitr_chapter8_plus
)

### Generating Module File
_generate_module_eus(agitr_chapter8_plus
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/agitr_chapter8_plus
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(agitr_chapter8_plus_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(agitr_chapter8_plus_generate_messages agitr_chapter8_plus_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_eus _agitr_chapter8_plus_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_eus _agitr_chapter8_plus_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(agitr_chapter8_plus_geneus)
add_dependencies(agitr_chapter8_plus_geneus agitr_chapter8_plus_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS agitr_chapter8_plus_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services
_generate_srv_lisp(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/agitr_chapter8_plus
)
_generate_srv_lisp(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/agitr_chapter8_plus
)

### Generating Module File
_generate_module_lisp(agitr_chapter8_plus
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/agitr_chapter8_plus
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(agitr_chapter8_plus_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(agitr_chapter8_plus_generate_messages agitr_chapter8_plus_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_lisp _agitr_chapter8_plus_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_lisp _agitr_chapter8_plus_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(agitr_chapter8_plus_genlisp)
add_dependencies(agitr_chapter8_plus_genlisp agitr_chapter8_plus_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS agitr_chapter8_plus_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages

### Generating Services
_generate_srv_nodejs(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/agitr_chapter8_plus
)
_generate_srv_nodejs(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/agitr_chapter8_plus
)

### Generating Module File
_generate_module_nodejs(agitr_chapter8_plus
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/agitr_chapter8_plus
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(agitr_chapter8_plus_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(agitr_chapter8_plus_generate_messages agitr_chapter8_plus_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_nodejs _agitr_chapter8_plus_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_nodejs _agitr_chapter8_plus_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(agitr_chapter8_plus_gennodejs)
add_dependencies(agitr_chapter8_plus_gennodejs agitr_chapter8_plus_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS agitr_chapter8_plus_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages

### Generating Services
_generate_srv_py(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/agitr_chapter8_plus
)
_generate_srv_py(agitr_chapter8_plus
  "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/agitr_chapter8_plus
)

### Generating Module File
_generate_module_py(agitr_chapter8_plus
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/agitr_chapter8_plus
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(agitr_chapter8_plus_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(agitr_chapter8_plus_generate_messages agitr_chapter8_plus_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changerate.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_py _agitr_chapter8_plus_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise5_ws/src/agitr_chapter8_plus/srv/Changespeed.srv" NAME_WE)
add_dependencies(agitr_chapter8_plus_generate_messages_py _agitr_chapter8_plus_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(agitr_chapter8_plus_genpy)
add_dependencies(agitr_chapter8_plus_genpy agitr_chapter8_plus_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS agitr_chapter8_plus_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/agitr_chapter8_plus)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/agitr_chapter8_plus
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(agitr_chapter8_plus_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/agitr_chapter8_plus)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/agitr_chapter8_plus
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(agitr_chapter8_plus_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/agitr_chapter8_plus)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/agitr_chapter8_plus
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(agitr_chapter8_plus_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/agitr_chapter8_plus)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/agitr_chapter8_plus
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(agitr_chapter8_plus_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/agitr_chapter8_plus)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/agitr_chapter8_plus\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/agitr_chapter8_plus
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(agitr_chapter8_plus_generate_messages_py std_msgs_generate_messages_py)
endif()
