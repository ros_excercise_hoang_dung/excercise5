//This program spawns a new turtlesim turtle by calling
// the appropriate service.
#include <ros/ros.h>
//The srv class for the service.
#include <turtlesim/Spawn.h>
#include <geometry_msgs/Twist.h>

ros::Publisher pub;

void Callback(const geometry_msgs::Twist& sub_msg)
{
    geometry_msgs::Twist pub_msg;
    pub_msg.linear.x = sub_msg.linear.x;
    pub_msg.angular.z = sub_msg.angular.z;
    pub.publish(pub_msg);
}

int main(int argc, char **argv){

    ros::init(argc, argv, "spawn_turtle");
    ros::NodeHandle nh;

//Create a client object for the spawn service. This
//needs to know the data type of the service and its name.
    ros::ServiceClient spawnClient
		= nh.serviceClient<turtlesim::Spawn>("spawn");

//Dang ki topic turtle1/cmd_vel
    ros::Subscriber sub = nh.subscribe("turtle1/cmd_vel",1000,&Callback);
//Xuat ban len topic Myturtle/cmd_vel
    pub = nh.advertise<geometry_msgs::Twist>("MyTurtle/cmd_vel",1000);
//Create the request and response objects.
    turtlesim::Spawn::Request req;
    turtlesim::Spawn::Response resp;

    req.x = 2;
    req.y = 3;
    req.theta = M_PI/2;
    req.name = "MyTurtle";

    ros::service::waitForService("spawn", ros::Duration(5));
    bool success = spawnClient.call(req,resp);

    if(success){
	ROS_INFO_STREAM("Spawned a turtle named "
			<< resp.name);
    }else{
	ROS_ERROR_STREAM("Failed to spawn.");
    }

    while(ros::ok())
    {
        ros::spinOnce();
    }
}
