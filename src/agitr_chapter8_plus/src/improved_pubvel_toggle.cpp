//this program toggles between rotation and translation
//commands,based on calls to a service.
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <agitr_chapter8_plus/Changerate.h>
#include <agitr_chapter8_plus/Changespeed.h>

bool forward = true;
bool start = true; // Bien nay cho biet cu rua co chay hau khong
double speed = 1.0; // Bien nay luu toc do cu rua
double newfrequency;
bool ratechanged = false;

bool toggleForward(
	std_srvs::Empty::Request &req,
	std_srvs::Empty::Response &resp){
    forward = !forward;
    ROS_INFO_STREAM("Now sending "<<(forward?
                "forward":"rotate")<< " commands.");
	return true;
}

bool changeRate(
        agitr_chapter8_plus::Changerate::Request &req,
        agitr_chapter8_plus::Changerate::Response &resp){

        ROS_INFO_STREAM("Changing rate to "<<req.newrate);

        newfrequency = req.newrate;
        ratechanged = true;
        return true;
}

bool StartStop(
    std_srvs::Empty::Request &req,
    std_srvs::Empty::Response &resp){
    start = !start;
    ROS_INFO_STREAM("Trang thai cu rua: " << (start?"start":"stop"));
    return true;
}

bool changeSpeed(
    agitr_chapter8_plus::Changespeed::Request &req,
    agitr_chapter8_plus::Changespeed::Response &resp){
    speed = req.newspeed;
    ROS_INFO_STREAM("Van toc cu rua duoc doi thanh: "<< req.newspeed);
    return true;
}


int main(int argc, char **argv){
    ros::init(argc,argv,"pubvel_toggle_rate");
	ros::NodeHandle nh;
        
	ros::ServiceServer server =  nh.advertiseService("toggle_forward",&toggleForward);
                
    ros::ServiceServer server0 = nh.advertiseService("change_rate",&changeRate);

    ros::ServiceServer server_startstop =  nh.advertiseService("start_stop",&StartStop);
                
    ros::ServiceServer server_changespeed = nh.advertiseService("change_speed",&changeSpeed);
                
    ros::Publisher pub=nh.advertise<geometry_msgs::Twist>(
		"turtle1/cmd_vel",1000);
    
        ros::Rate rate(2);
	while(ros::ok()){
		geometry_msgs::Twist msg;
        if (start)
        {
            msg.linear.x = speed*(forward?1.0:0.0);
            msg.angular.z = speed*(forward?0.0:1.0);
        }else{
            msg.linear.x = 0;
            msg.angular.z = 0;
        }
        
		pub.publish(msg);
		ros::spinOnce();
        if(ratechanged) {
            rate = ros::Rate(newfrequency);
            ratechanged = false;
        }
		rate.sleep();
	}
}
