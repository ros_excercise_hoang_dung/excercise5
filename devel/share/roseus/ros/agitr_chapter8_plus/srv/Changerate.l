;; Auto-generated. Do not edit!


(when (boundp 'agitr_chapter8_plus::Changerate)
  (if (not (find-package "AGITR_CHAPTER8_PLUS"))
    (make-package "AGITR_CHAPTER8_PLUS"))
  (shadow 'Changerate (find-package "AGITR_CHAPTER8_PLUS")))
(unless (find-package "AGITR_CHAPTER8_PLUS::CHANGERATE")
  (make-package "AGITR_CHAPTER8_PLUS::CHANGERATE"))
(unless (find-package "AGITR_CHAPTER8_PLUS::CHANGERATEREQUEST")
  (make-package "AGITR_CHAPTER8_PLUS::CHANGERATEREQUEST"))
(unless (find-package "AGITR_CHAPTER8_PLUS::CHANGERATERESPONSE")
  (make-package "AGITR_CHAPTER8_PLUS::CHANGERATERESPONSE"))

(in-package "ROS")





(defclass agitr_chapter8_plus::ChangerateRequest
  :super ros::object
  :slots (_newrate ))

(defmethod agitr_chapter8_plus::ChangerateRequest
  (:init
   (&key
    ((:newrate __newrate) 0.0)
    )
   (send-super :init)
   (setq _newrate (float __newrate))
   self)
  (:newrate
   (&optional __newrate)
   (if __newrate (setq _newrate __newrate)) _newrate)
  (:serialization-length
   ()
   (+
    ;; float64 _newrate
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _newrate
       (sys::poke _newrate (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _newrate
     (setq _newrate (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass agitr_chapter8_plus::ChangerateResponse
  :super ros::object
  :slots (_ret ))

(defmethod agitr_chapter8_plus::ChangerateResponse
  (:init
   (&key
    ((:ret __ret) nil)
    )
   (send-super :init)
   (setq _ret __ret)
   self)
  (:ret
   (&optional (__ret :null))
   (if (not (eq __ret :null)) (setq _ret __ret)) _ret)
  (:serialization-length
   ()
   (+
    ;; bool _ret
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _ret
       (if _ret (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _ret
     (setq _ret (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass agitr_chapter8_plus::Changerate
  :super ros::object
  :slots ())

(setf (get agitr_chapter8_plus::Changerate :md5sum-) "4c8b4fd3f5e2b2736a3c66de5e015351")
(setf (get agitr_chapter8_plus::Changerate :datatype-) "agitr_chapter8_plus/Changerate")
(setf (get agitr_chapter8_plus::Changerate :request) agitr_chapter8_plus::ChangerateRequest)
(setf (get agitr_chapter8_plus::Changerate :response) agitr_chapter8_plus::ChangerateResponse)

(defmethod agitr_chapter8_plus::ChangerateRequest
  (:response () (instance agitr_chapter8_plus::ChangerateResponse :init)))

(setf (get agitr_chapter8_plus::ChangerateRequest :md5sum-) "4c8b4fd3f5e2b2736a3c66de5e015351")
(setf (get agitr_chapter8_plus::ChangerateRequest :datatype-) "agitr_chapter8_plus/ChangerateRequest")
(setf (get agitr_chapter8_plus::ChangerateRequest :definition-)
      "float64 newrate
---
bool ret
")

(setf (get agitr_chapter8_plus::ChangerateResponse :md5sum-) "4c8b4fd3f5e2b2736a3c66de5e015351")
(setf (get agitr_chapter8_plus::ChangerateResponse :datatype-) "agitr_chapter8_plus/ChangerateResponse")
(setf (get agitr_chapter8_plus::ChangerateResponse :definition-)
      "float64 newrate
---
bool ret
")



(provide :agitr_chapter8_plus/Changerate "4c8b4fd3f5e2b2736a3c66de5e015351")


