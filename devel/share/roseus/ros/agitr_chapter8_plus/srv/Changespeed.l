;; Auto-generated. Do not edit!


(when (boundp 'agitr_chapter8_plus::Changespeed)
  (if (not (find-package "AGITR_CHAPTER8_PLUS"))
    (make-package "AGITR_CHAPTER8_PLUS"))
  (shadow 'Changespeed (find-package "AGITR_CHAPTER8_PLUS")))
(unless (find-package "AGITR_CHAPTER8_PLUS::CHANGESPEED")
  (make-package "AGITR_CHAPTER8_PLUS::CHANGESPEED"))
(unless (find-package "AGITR_CHAPTER8_PLUS::CHANGESPEEDREQUEST")
  (make-package "AGITR_CHAPTER8_PLUS::CHANGESPEEDREQUEST"))
(unless (find-package "AGITR_CHAPTER8_PLUS::CHANGESPEEDRESPONSE")
  (make-package "AGITR_CHAPTER8_PLUS::CHANGESPEEDRESPONSE"))

(in-package "ROS")





(defclass agitr_chapter8_plus::ChangespeedRequest
  :super ros::object
  :slots (_newspeed ))

(defmethod agitr_chapter8_plus::ChangespeedRequest
  (:init
   (&key
    ((:newspeed __newspeed) 0.0)
    )
   (send-super :init)
   (setq _newspeed (float __newspeed))
   self)
  (:newspeed
   (&optional __newspeed)
   (if __newspeed (setq _newspeed __newspeed)) _newspeed)
  (:serialization-length
   ()
   (+
    ;; float64 _newspeed
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _newspeed
       (sys::poke _newspeed (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _newspeed
     (setq _newspeed (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass agitr_chapter8_plus::ChangespeedResponse
  :super ros::object
  :slots (_ret ))

(defmethod agitr_chapter8_plus::ChangespeedResponse
  (:init
   (&key
    ((:ret __ret) nil)
    )
   (send-super :init)
   (setq _ret __ret)
   self)
  (:ret
   (&optional (__ret :null))
   (if (not (eq __ret :null)) (setq _ret __ret)) _ret)
  (:serialization-length
   ()
   (+
    ;; bool _ret
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _ret
       (if _ret (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _ret
     (setq _ret (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass agitr_chapter8_plus::Changespeed
  :super ros::object
  :slots ())

(setf (get agitr_chapter8_plus::Changespeed :md5sum-) "d7e28d0cca4452e86706e354055b7abc")
(setf (get agitr_chapter8_plus::Changespeed :datatype-) "agitr_chapter8_plus/Changespeed")
(setf (get agitr_chapter8_plus::Changespeed :request) agitr_chapter8_plus::ChangespeedRequest)
(setf (get agitr_chapter8_plus::Changespeed :response) agitr_chapter8_plus::ChangespeedResponse)

(defmethod agitr_chapter8_plus::ChangespeedRequest
  (:response () (instance agitr_chapter8_plus::ChangespeedResponse :init)))

(setf (get agitr_chapter8_plus::ChangespeedRequest :md5sum-) "d7e28d0cca4452e86706e354055b7abc")
(setf (get agitr_chapter8_plus::ChangespeedRequest :datatype-) "agitr_chapter8_plus/ChangespeedRequest")
(setf (get agitr_chapter8_plus::ChangespeedRequest :definition-)
      "float64 newspeed
---
bool ret
")

(setf (get agitr_chapter8_plus::ChangespeedResponse :md5sum-) "d7e28d0cca4452e86706e354055b7abc")
(setf (get agitr_chapter8_plus::ChangespeedResponse :datatype-) "agitr_chapter8_plus/ChangespeedResponse")
(setf (get agitr_chapter8_plus::ChangespeedResponse :definition-)
      "float64 newspeed
---
bool ret
")



(provide :agitr_chapter8_plus/Changespeed "d7e28d0cca4452e86706e354055b7abc")


