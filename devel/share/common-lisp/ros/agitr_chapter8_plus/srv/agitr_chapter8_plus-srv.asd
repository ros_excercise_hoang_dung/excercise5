
(cl:in-package :asdf)

(defsystem "agitr_chapter8_plus-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Changerate" :depends-on ("_package_Changerate"))
    (:file "_package_Changerate" :depends-on ("_package"))
    (:file "Changespeed" :depends-on ("_package_Changespeed"))
    (:file "_package_Changespeed" :depends-on ("_package"))
  ))