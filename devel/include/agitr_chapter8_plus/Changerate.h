// Generated by gencpp from file agitr_chapter8_plus/Changerate.msg
// DO NOT EDIT!


#ifndef AGITR_CHAPTER8_PLUS_MESSAGE_CHANGERATE_H
#define AGITR_CHAPTER8_PLUS_MESSAGE_CHANGERATE_H

#include <ros/service_traits.h>


#include <agitr_chapter8_plus/ChangerateRequest.h>
#include <agitr_chapter8_plus/ChangerateResponse.h>


namespace agitr_chapter8_plus
{

struct Changerate
{

typedef ChangerateRequest Request;
typedef ChangerateResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct Changerate
} // namespace agitr_chapter8_plus


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::agitr_chapter8_plus::Changerate > {
  static const char* value()
  {
    return "4c8b4fd3f5e2b2736a3c66de5e015351";
  }

  static const char* value(const ::agitr_chapter8_plus::Changerate&) { return value(); }
};

template<>
struct DataType< ::agitr_chapter8_plus::Changerate > {
  static const char* value()
  {
    return "agitr_chapter8_plus/Changerate";
  }

  static const char* value(const ::agitr_chapter8_plus::Changerate&) { return value(); }
};


// service_traits::MD5Sum< ::agitr_chapter8_plus::ChangerateRequest> should match
// service_traits::MD5Sum< ::agitr_chapter8_plus::Changerate >
template<>
struct MD5Sum< ::agitr_chapter8_plus::ChangerateRequest>
{
  static const char* value()
  {
    return MD5Sum< ::agitr_chapter8_plus::Changerate >::value();
  }
  static const char* value(const ::agitr_chapter8_plus::ChangerateRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::agitr_chapter8_plus::ChangerateRequest> should match
// service_traits::DataType< ::agitr_chapter8_plus::Changerate >
template<>
struct DataType< ::agitr_chapter8_plus::ChangerateRequest>
{
  static const char* value()
  {
    return DataType< ::agitr_chapter8_plus::Changerate >::value();
  }
  static const char* value(const ::agitr_chapter8_plus::ChangerateRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::agitr_chapter8_plus::ChangerateResponse> should match
// service_traits::MD5Sum< ::agitr_chapter8_plus::Changerate >
template<>
struct MD5Sum< ::agitr_chapter8_plus::ChangerateResponse>
{
  static const char* value()
  {
    return MD5Sum< ::agitr_chapter8_plus::Changerate >::value();
  }
  static const char* value(const ::agitr_chapter8_plus::ChangerateResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::agitr_chapter8_plus::ChangerateResponse> should match
// service_traits::DataType< ::agitr_chapter8_plus::Changerate >
template<>
struct DataType< ::agitr_chapter8_plus::ChangerateResponse>
{
  static const char* value()
  {
    return DataType< ::agitr_chapter8_plus::Changerate >::value();
  }
  static const char* value(const ::agitr_chapter8_plus::ChangerateResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // AGITR_CHAPTER8_PLUS_MESSAGE_CHANGERATE_H
