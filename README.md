Ex 5.1 a

Tạo một file copy từ pubvel_toggle_plus.cpp và đặt tên nó là improved_pubvel_toggle.cpp, thay đổi file mới bao gồm

Một service để bắt đầu và dừng cụ rùa

Một service cho phép thay đổi tốc độ cụ rùa

Ex 5.1 b

Tạo một file copy từ spawn_turtle.cpp và đặt tên nó là spawn_turtle_plus.cpp, thay đổi file mới: bên cạnh việc gọi service /spawn để triệu hồi cụ rùa MyTurtle thì node này sẽ:

Đăng kí topic turtle1/cmd_vel

Gửi vận tốc nhận được cho topic MyTurtle/cmd_vel để chạy hai cụ rùa một cách đồng thời

Ex 5.1 c

Tạo file launch:

Chạy turtlesim và improved_pubvel_toggle

Chạy spawn_turtle_plus sử dụng argument
